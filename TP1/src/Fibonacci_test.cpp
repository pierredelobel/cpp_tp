#include "Fibonacci.h"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupFibonacci) { };
TEST(GroupFibonacci, test_fibonacci_1) { // premier test unitaire
    int result = fibonacciIteratif(10);
    CHECK_EQUAL(55, result);
}
TEST(GroupFibonacci, test_fibonacci_2) { // deuxième test unitaire
    int result = fibonacciRecursif(10);
    CHECK_EQUAL(55, result);
}