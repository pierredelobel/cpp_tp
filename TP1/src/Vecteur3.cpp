#include "Vecteur3.h"
#include <iostream>
#include <cmath>

Vecteur3::Vecteur3(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

Vecteur3::~Vecteur3()
{
}

void Vecteur3::afficher() {
    std::cout << "(" << this->x << ", " << this->y << ", " << this->z << ")" << std::endl;
}

float Vecteur3::norme() {
    return std::sqrt(std::pow(this->x, 2) + std::pow(this->y, 2) + std::pow(this->z, 2));
}

float Vecteur3::produitScalaire(Vecteur3 v3) {
    return (this->x * v3.x + this->y * v3.y + this->z * v3.z);
}

Vecteur3 Vecteur3::addition(Vecteur3 v3){
    Vecteur3 v(this->x + v3.x, this->y + v3.y, this->z + v3.z);
    return v;
}

float Vecteur3::getX() {
    return this->x;
}

float Vecteur3::getY() {
    return this->y;
}

float Vecteur3::getZ() {
    return this->z;
}

