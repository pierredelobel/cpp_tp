class Vecteur3
{
private:
    float x,y,z;
public:
    Vecteur3(float x, float y, float z);
    ~Vecteur3();
    void afficher();
    float norme();
    float produitScalaire(Vecteur3 v3);
    Vecteur3 addition(Vecteur3 v3);
    float getX();
    float getY();
    float getZ();
};