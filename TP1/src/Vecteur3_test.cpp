#include "Vecteur3.h"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupVecteur3) { };
TEST(GroupVecteur3, test_vecteur3_1) { // premier test unitaire
    Vecteur3 v3(2,3,6);
    float result = v3.norme();
    CHECK_EQUAL(7, result);
}
TEST(GroupVecteur3, test_vecteur3_2) { // deuxième test unitaire
    Vecteur3 v3_1(2,3,4);
    Vecteur3 v3_2(1,1,1);
    int result = v3_1.produitScalaire(v3_2);
    CHECK_EQUAL(9, result);
}
TEST(GroupVecteur3, test_vecteur3_3) { // deuxième test unitaire
    Vecteur3 v3_1(2,3,4);
    Vecteur3 v3_2(1,1,1);
    Vecteur3 result = v3_1.addition(v3_2);
    CHECK_EQUAL(3, result.getX());
    CHECK_EQUAL(4, result.getY());
    CHECK_EQUAL(5, result.getZ());
}