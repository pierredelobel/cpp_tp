#include <iostream>
#include "Fibonacci.h"
#include "Vecteur3.h"

int main() {
    //std::cout << "Hello world !" << std::endl;
    std::cout << fibonacciIteratif(10) << std::endl;
    std::cout << fibonacciRecursif(10) << std::endl;
    Vecteur3 v3(2,3,6);
    v3.afficher();
    return 0;
}