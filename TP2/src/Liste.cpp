#include "Liste.hpp"
#include <iostream>

Liste::Liste(/* args */)
{
  this->_tete = nullptr;
}

Liste::~Liste()
{
  Noeud* p = this->_tete;
  while(this->_tete != nullptr){
    this->_tete = this->_tete->_suivant;
    delete p;
    p = this->_tete;
  }
  
}

void Liste::ajouterDevant(int valeur) {
  Noeud* n = new Noeud(valeur, this->_tete);
  this->_tete = n;
}

int Liste::getElement(int indice) {
  Noeud* p = this->_tete;
  for(int i = 0; i < indice; i++)
  {
    p = p->_suivant;
  }

  return p->_valeur;
}

int Liste::getTaille() {
  Noeud* p = this->_tete;
  int n = 0;
  while(p != nullptr)
  {
    n++;
    p = p->_suivant;
  }
  return n;
}
