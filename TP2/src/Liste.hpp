#ifndef LISTE_HPP_
#define LISTE_HPP_
#include "Noeud.hpp"

class Liste
{
private:
    
public:
    Noeud* _tete;
    Liste();
    ~Liste();
    void ajouterDevant(int valeur);
    int getTaille();
    int getElement(int indice);
};

#endif
