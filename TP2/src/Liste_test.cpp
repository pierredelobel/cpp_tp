#include "Liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupDoubler) { };

TEST(GroupDoubler, Liste_ajouterDevant)  {
    Liste* l = new Liste();
    l->ajouterDevant(111);
    l->ajouterDevant(222);
    CHECK_EQUAL(l->_tete->_valeur, 222);
}

TEST(GroupDoubler, Liste_getElement)  {
    Liste* l = new Liste();
    l->ajouterDevant(111);
    l->ajouterDevant(222);
    CHECK_EQUAL(l->getElement(1), 111);
}

TEST(GroupDoubler, Liste_getTaille)  {
    Liste* l = new Liste();
    l->ajouterDevant(111);
    l->ajouterDevant(222);
    CHECK_EQUAL(l->getTaille(), 2);
}

