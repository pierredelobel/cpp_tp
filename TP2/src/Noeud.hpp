#ifndef NOEUD_HPP_
#define NOEUD_HPP_

class Noeud{

public: 
    int _valeur;
    Noeud* _suivant;
    Noeud(int valeur, Noeud* n);
};

#endif
