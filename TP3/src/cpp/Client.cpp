#include "../hpp/Client.hpp"
#include <iostream>

Client::Client(int _id, const std::string &_nom) : _id(_id), _nom(_nom) {}

Client::~Client() {
}

int Client::getId() const{
    return this->_id;
}

const std::string& Client::getNom() const {
    return this->_nom;
}

void Client::afficherClient() const{
    std::cout <<
    "Client (" <<
    getId() <<
    ", " <<
    getNom() <<
    ")" <<
    std::endl;
}