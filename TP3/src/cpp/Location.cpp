#include "../hpp/Location.hpp"
#include <iostream>

Location::Location(int _idClient, int _idProduit) : _idClient(_idClient), _idProduit(_idProduit) {}

void Location::afficherLocation() const{
    std::cout << 
    "Location (" << 
    this->_idClient << 
    ", " << 
    this->_idProduit << 
    ")" << 
    std::endl;
}

