#include "../hpp/Magasin.hpp"
#include <iostream>
#include <algorithm>

Magasin::Magasin()
{
    this->_idCourantClient = 0;
    this->_idCourantProduit = 0;
}

int Magasin::nbClients() const
{
    return this->_clients.size();
}

void Magasin::ajouterClient(const std::string &nom)
{
    Client c(this->_idCourantClient, nom);
    this->_clients.push_back(c);
    this->_idCourantClient++;
}

void Magasin::afficherClients() const
{
    std::vector<Client>::const_iterator iterator = this->_clients.begin();
    while (iterator != this->_clients.end())
    {
        iterator->afficherClient();
        ++iterator;
    }
}

void Magasin::supprimerClient(int idClient)
{
    int find = 0;
    std::vector<Client>::iterator iterator = this->_clients.begin();
    while (iterator != this->_clients.end())
    {
        if (iterator->getId() == idClient)
        {
            this->_clients.erase(iterator);
            find = 1;
        }
        ++iterator;
    }

    if (find == 0)
    {
        throw std::string("erreur: ce client n'existe pas");
    }
}

int Magasin::nbProduits() const
{
    return this->_produits.size();
}

void Magasin::ajouterProduit(const std::string &nom)
{
    Produit p(this->_idCourantProduit, nom);
    this->_produits.push_back(p);
    this->_idCourantProduit++;
}

void Magasin::afficherProduits() const
{
    std::vector<Produit>::const_iterator iterator = this->_produits.begin();
    while (iterator != this->_produits.end())
    {
        iterator->afficherProduit();
        ++iterator;
    }
}

void Magasin::supprimerProduit(int idProduit)
{
    int find = 0;
    std::vector<Produit>::iterator iterator = this->_produits.begin();
    while (iterator != this->_produits.end())
    {
        if (iterator->getId() == idProduit)
        {
            this->_produits.erase(iterator);
            find = 1;
        }
        ++iterator;
    }

    if (find == 0)
    {
        throw std::string("erreur: ce produit n'existe pas");
    }
}

int Magasin::nbLocations() const
{
    return this->_locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit)
{
    std::vector<Location>::const_iterator iterator = this->_locations.begin();
    while (iterator != this->_locations.end())
    {
        if (iterator->_idClient == idClient && iterator->_idProduit == idProduit)
        {
            throw std::string("la location existe déjà");
        }

        ++iterator;
    }
    Location l(idClient, idProduit);
    this->_locations.push_back(l);
}

void Magasin::afficherLocations() const
{
    std::vector<Location>::const_iterator iterator = this->_locations.begin();
    while (iterator != this->_locations.end())
    {
        iterator->afficherLocation();
        ++iterator;
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit)
{
    int find = 0;
    std::vector<Location>::iterator iterator = this->_locations.begin();
    while (iterator != this->_locations.end())
    {
        if (iterator->_idClient == idClient && iterator->_idProduit != idProduit)
        {
            this->_locations.erase(iterator);
            find = 1;
        }
        ++iterator;
    }

    if (find == 0)
    {
        throw std::string("erreur: cette location n'existe pas");
    }
}

bool Magasin::trouverClientDansLocation(int idClient) const
{
    std::vector<Location>::const_iterator iterator = this->_locations.begin();
    while (iterator != this->_locations.end())
    {
        if (iterator->_idClient == idClient)
        {
            return true;
        }

        ++iterator;
    }

    return false;
}

std::vector<int> Magasin::calculerClientsLibres() const
{
    std::vector<int> clientsLibres;
    std::vector<Client>::const_iterator iteratorClient = this->_clients.begin();
    std::vector<Location>::const_iterator iteratorLocation = this->_locations.begin();
    while (iteratorClient != this->_clients.end())
    {
        int nonLibre = 0;
        while (iteratorLocation != this->_locations.end())
        {
            if (iteratorClient->getId() == iteratorLocation->_idClient)
            {
                nonLibre = 1;
            }
            iteratorLocation++;
        }
        if (nonLibre == 0)
        {
            clientsLibres.push_back(iteratorClient->getId());
            nonLibre = 0;
        }
        iteratorLocation = this->_locations.begin();
        ++iteratorClient;
    }
    return clientsLibres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const
{
    std::vector<Location>::const_iterator iterator = this->_locations.begin();
    while (iterator != this->_locations.end())
    {
        if (iterator->_idProduit == idProduit)
        {
            return true;
        }

        ++iterator;
    }

    return false;
}

std::vector<int> Magasin::calculerProduitsLibres() const
{
    std::vector<int> produitsLibres;
    std::vector<Produit>::const_iterator iteratorProduit = this->_produits.begin();
    std::vector<Location>::const_iterator iteratorLocation = this->_locations.begin();
    while (iteratorProduit != this->_produits.end())
    {
        int nonLibre = 0;
        while (iteratorLocation != this->_locations.end())
        {
            if (iteratorProduit->getId() == iteratorLocation->_idClient)
            {
                nonLibre = 1;
            }
            iteratorLocation++;
        }
        if (nonLibre == 0)
        {
            produitsLibres.push_back(iteratorProduit->getId());
            nonLibre = 0;
        }
        iteratorLocation = this->_locations.begin();
        ++iteratorProduit;
    }
    return produitsLibres;
}