#include "../hpp/Produit.hpp"
#include <iostream>

Produit::Produit(int _id, const std::string &_description) : _id(_id), _description(_description) {}

Produit::~Produit() {
}

int Produit::getId() const{
    return this->_id;
}

const std::string& Produit::getDescription() const{
    return this->_description;
}

void Produit::afficherProduit() const {
    std::cout << 
    "Produit (" <<
    getId() <<
    " ," <<
    getDescription() <<
    ")" <<
    std::endl;
}