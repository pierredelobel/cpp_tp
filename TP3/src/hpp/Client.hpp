#ifndef CLIENT_HPP_
#define CLIENT_HPP_
#include <iostream>

class Client
{
    public:
        // variables
        int _id;
        std::string _nom;

        // constructeur / destucteur
        Client(int _id, const std::string &_nom);
        ~Client();

        // accesseurs
        int getId() const; 
        const std::string& getNom() const;

        // méthodes
        void afficherClient() const;
};

#endif
