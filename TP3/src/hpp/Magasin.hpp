#ifndef MAGASIN_HPP_
#define MAGASIN_HPP_
#include <iostream>
#include <vector>
#include "Client.hpp"
#include "Produit.hpp"
#include "Location.hpp"

using namespace std;

class Magasin
{
    public:
        vector<Client> _clients;
        vector<Produit> _produits;
        vector<Location> _locations;
        int _idCourantClient;
        int _idCourantProduit;
        string _description;
        Magasin();
        int nbClients() const;
        void ajouterClient(const std::string &nom);
        void afficherClients() const;
        void supprimerClient(int idClient);

        int nbProduits() const;
        void ajouterProduit(const std::string &nom);
        void afficherProduits() const;
        void supprimerProduit(int idProduit);

        int nbLocations() const;
        void ajouterLocation(int idClient, int idProduit);
        void afficherLocations() const;
        void supprimerLocation(int idClient, int idProduit);

        bool trouverClientDansLocation(int idClient) const;
        vector<int> calculerClientsLibres() const;
        bool trouverProduitDansLocation(int idProduit) const;
        vector<int> calculerProduitsLibres() const;
};

#endif
