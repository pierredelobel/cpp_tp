#ifndef PRODUIT_HPP_
#define PRODUIT_HPP_
#include <iostream>

class Produit
{
    public:
        int _id;
        std::string _description;
        Produit(int _id, const std::string &_description);
        ~Produit();
        int getId() const;
        const std::string& getDescription() const;
        void afficherProduit() const;
};

#endif
