#include "hpp/Location.hpp"
#include "hpp/Client.hpp"
#include "hpp/Produit.hpp"

#include <iostream>

int main() {
    Location l(0, 2);
    l.afficherLocation();

    Client c(42, "toto");
    c.afficherClient();

    Produit p(1, "produit");
    p.afficherProduit();
    
    return 0;
}

