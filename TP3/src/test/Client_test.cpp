#include "../hpp/Client.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, Client_test1)  {
    Client c(42, "toto");
    CHECK_EQUAL(c.getId(), 42);
}

TEST(GroupClient, Client_test2)  {
    Client c(42, "toto");
    CHECK_EQUAL(c.getNom(), "toto");
}