#include "../hpp/Magasin.hpp"
#include <vector>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin){};

TEST(GroupMagasin, Magasin_test1)
{
    Magasin mag;
    mag.ajouterProduit("produit");
    CHECK_EQUAL(mag.nbProduits(), 1);
}

TEST(GroupMagasin, Magasin_test2)
{
    Magasin mag;
    mag.ajouterClient("julien");
    mag.ajouterClient("david");
    CHECK_EQUAL(mag.nbClients(), 2);
}

TEST(GroupMagasin, Magasin_test3)
{
    Magasin mag;
    CHECK_THROWS(std::string ,mag.supprimerClient(1));  
}

TEST(GroupMagasin, Magasin_test4)
{
    Magasin mag;
    CHECK_THROWS(std::string, mag.supprimerProduit(1));
}

TEST(GroupMagasin, Magasin_test5)
{
    Magasin mag;
    std::vector<int> clients;
    mag.ajouterLocation(0, 1);
    mag.ajouterClient("pierre");
    CHECK_EQUAL(mag.calculerClientsLibres().size(), clients.size());
}

TEST(GroupMagasin, Magasin_test6)
{
    Magasin mag;
    std::vector<int> produits;
    mag.ajouterLocation(0, 0);
    mag.ajouterProduit("produit");
    CHECK_EQUAL(mag.calculerProduitsLibres().size(), produits.size());
}
