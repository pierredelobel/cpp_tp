#include "../hpp/PolygoneRegulier.hpp"
#define PI 3.14159265
#include <ctgmath>
#include <iostream>

using namespace std;

//angle = 2*PI / nbCotes;

//x = centre._x +  rayon * cos(angle* i)
//y = centre._y +  rayon * sin(angle* i)

//où i est le numero de chacun des points (de 0 à nbCotes - 1)

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes) : 
    FigureGeometrique(couleur),  
    _nbPoints(nbCotes),
    _points(new Point[nbCotes])
{
    float delta = 2.f * PI / nbCotes;
    for (int i = 0; i < nbCotes; i++) {
        _points[i] = Point{
            static_cast<int>(centre._x +  rayon * cos(delta * i)),
            static_cast<int>(centre._y + rayon * sin(delta * i))
        };
    }
}

PolygoneRegulier::~PolygoneRegulier()
{
    delete[] _points;
}

void PolygoneRegulier::afficher() const
{
    cout <<
    "PolygoneRegulier " <<
    this->_couleur._r <<
    "_" <<
    this->getCouleur()._g << 
    "_" << 
    this->_couleur._b << 
    " " ;

    for(int i = 0; i < this->_nbPoints; i++)
    {
        cout <<
        this->_points[i]._x <<
        "_" <<
        this->_points[i]._y <<
        " ";
    }
    cout << endl;
    
}