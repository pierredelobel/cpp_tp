#ifndef POLYGONEREGULIER_HPP_
#define POLYGONEREGULIER_HPP_

#include "Point.hpp"
#include "Couleur.hpp"
#include "FigureGeometrique.hpp"

using namespace std;

class PolygoneRegulier : public FigureGeometrique
{
private:
    int _nbPoints;
    Point * _points;
public:
    PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes);
    virtual ~PolygoneRegulier();
    void afficher() const;
};

#endif