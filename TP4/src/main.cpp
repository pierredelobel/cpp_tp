#include "hpp/FigureGeometrique.hpp"
#include "hpp/Point.hpp"
#include "hpp/Ligne.hpp"
#include "hpp/PolygoneRegulier.hpp"

#include <iostream>
#include <vector>

int main() {
    Couleur c = {
        1.0,
        0.0,
        0.0
    };
    Point p0 = {
        0,
        0
    };

    Point p1 = {
        100,
        200
    };
    Ligne l(c, p0, p1);
    // l.afficher();

    FigureGeometrique* f = new PolygoneRegulier(c, p1, 50, 5);
    delete f;

    PolygoneRegulier p(c, p1, 50, 5);
    // p.afficher();

    std::vector<FigureGeometrique *> fig;
    fig.push_back(&l);
    fig.push_back(&p);

}

