#include "../hpp/FigureGeometrique.hpp"

FigureGeometrique::FigureGeometrique(const Couleur & couleur) : _couleur(couleur){}

FigureGeometrique::~FigureGeometrique(){}

const Couleur & FigureGeometrique::getCouleur() const
{
    return this->_couleur;
}