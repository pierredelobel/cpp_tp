#include "../hpp/Ligne.hpp"
#include "../hpp/FigureGeometrique.hpp"
#include <iostream>
using namespace std;

Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1) : FigureGeometrique(couleur), _p0(p0), _p1(p1) {}

void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> ctx) const
{
    ctx->set_source_rgb(this->getCouleur()._r,this->getCouleur()._g,this->getCouleur()._b);
    ctx->move_to(this->_p0._x, this->_p0._y);
    ctx->line_to(this->_p1._x, this->_p1._y);
    // cout <<
    // "Ligne " << 
    // this->_couleur._r <<
    // "_" <<
    // this->getCouleur()._g << 
    // "_" << 
    // this->_couleur._b << 
    // " " <<
    // this->_p0._x << 
    // "_" <<
    // this->_p0._y << 
    // " " << 
    // this->_p1._x <<
    // "_" <<
    // this->_p1._y <<
    // endl;

    ctx->stroke();

}

const Point & Ligne::getP0() const
{
    return this->_p0;
}

const Point & Ligne::getP1() const
{
    return this->_p1;
}
