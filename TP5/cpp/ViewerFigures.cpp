#include "../hpp/ViewerFigures.hpp"
#include "../hpp/ZoneDessin.hpp"

ViewerFigures::ViewerFigures(int argc, char ** argv) : _kit(argc, argv)
{
    this->_window.set_title("LE FAMEUX SNAKE");
    this->_window.set_default_size(640, 480);
}

ViewerFigures::~ViewerFigures()
{
}

void ViewerFigures::run()
{
    this->_window.add(z);
    this->_window.show_all();
    this->_kit.run(this->_window);
}