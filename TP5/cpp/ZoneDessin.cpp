#include "../hpp/ZoneDessin.hpp"
#include "../hpp/Ligne.hpp"
#include "../hpp/Couleur.hpp"
#include "../hpp/Point.hpp"
#include "../hpp/PolygoneRegulier.hpp"

#include <iostream>
#include <cstdlib>

ZoneDessin::ZoneDessin()
{
    add_events(Gdk::BUTTON_PRESS_MASK);
    this->signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));
}

ZoneDessin::~ZoneDessin()
{
    std::vector<FigureGeometrique*>::iterator iterator = this->_figures.begin();
    while (iterator != this->_figures.end())
    {
        FigureGeometrique* f = *iterator;
        this->_figures.erase(iterator);
        delete f;
    }
}

bool ZoneDessin::on_expose_event(GdkEventExpose* event)
{
    std::vector<FigureGeometrique*>::const_iterator iterator = this->_figures.begin();
    while (iterator != this->_figures.end())
    {
        const Cairo::RefPtr<Cairo::Context> ctx = this->get_window()->create_cairo_context();
        (*iterator)->afficher(ctx);
        iterator++;
    }
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton* event)
{
    double r = float(std::rand())/RAND_MAX*(1);
    double g = float(std::rand())/RAND_MAX*(1);
    double b = float(std::rand())/RAND_MAX*(1);
    Couleur c = {
        r,
        g,
        b
    };

    Point p1 = {
        (int)event->x,
        (int)event->y
    };
    int nbCotes = float(std::rand())/RAND_MAX*(7)+3;
    int rayon = float(std::rand())/RAND_MAX*(195)+5;
    if (event->button == 1) {
        this->_figures.push_back(new PolygoneRegulier(c, p1, rayon, nbCotes));
    } else {
        if (this->_figures.size() != 0) {
            delete this->_figures.back();
            this->_figures.pop_back();
        }
    }
    queue_draw();
    
    return true;
}