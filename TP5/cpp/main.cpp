#include <gtkmm.h>
#include "../hpp/ViewerFigures.hpp"
#include "../hpp/ZoneDessin.hpp"

int main(int argc, char ** argv) {
    ZoneDessin z();
    ViewerFigures view(argc, argv); 
    view.run();


}