#ifndef COULEUR_HPP_
#define COULEUR_HPP_

using namespace std;

struct Couleur
{
    double _r;
    double _g;
    double _b;
};

#endif
