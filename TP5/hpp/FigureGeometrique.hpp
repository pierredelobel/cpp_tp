#ifndef FIGUREGEOMETRIQUE_HPP_
#define FIGUREGEOMETRIQUE_HPP_
#include "Couleur.hpp"
#include <gtkmm.h>

using namespace std;

class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur & couleur);
    virtual ~FigureGeometrique();
    const Couleur & getCouleur() const;
    virtual void afficher(const Cairo::RefPtr<Cairo::Context> ctx) const = 0;
};
#endif
