#ifndef POLYGONEREGULIER_HPP_
#define POLYGONEREGULIER_HPP_

#include "Point.hpp"
#include "Couleur.hpp"
#include "FigureGeometrique.hpp"
#include <gtkmm.h>

using namespace std;

class PolygoneRegulier : public FigureGeometrique
{
private:
    int _nbPoints;
    Point * _points;
public:
    PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes);
    virtual ~PolygoneRegulier();
    void afficher(const Cairo::RefPtr<Cairo::Context> ctx) const;
};

#endif