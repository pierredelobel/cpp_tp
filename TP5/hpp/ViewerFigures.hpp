#ifndef VIEWERFIGURE_HPP_
#define VIEWERFIGURE_HPP_
#include <gtkmm.h>
#include "../hpp/ZoneDessin.hpp"

class ViewerFigures
{
private:
    Gtk::Main _kit;
    Gtk::Window _window;
    ZoneDessin z;
public:
    ViewerFigures(int argc, char ** argv);
    ~ViewerFigures();
    void run();

};

#endif
