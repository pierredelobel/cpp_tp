#ifndef ZONEDESSIN_HPP_
#define ZONEDESSIN_HPP_
#include <gtkmm.h>
#include <vector>
#include <stdio.h>
#include "FigureGeometrique.hpp"

class ZoneDessin : public Gtk::DrawingArea
{
private:
    std::vector<FigureGeometrique*> _figures;
    bool on_expose_event(GdkEventExpose* event);
    bool gererClic(GdkEventButton* event);
public:
    ZoneDessin();
    ~ZoneDessin();
    
};

#endif
