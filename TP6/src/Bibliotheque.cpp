#include "Bibliotheque.hpp"
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <iterator>

void Bibliotheque::afficher() const
{
    std::vector<Livre>::const_iterator iterator = this->begin();
    while (iterator != this->end())
    {
        std::cout << " titre: " << iterator->getTitre() << " auteur: " << iterator->getAuteur() << " annee : " << iterator->getAnnee() << std::endl;

        iterator++;
    }
}

void Bibliotheque::trierParAuteurEtTitre()
{
    std::sort(this->begin(), this->end());
}

void Bibliotheque::trierParAnnee()
{
    std::sort(this->begin(), this->end(), [](const Livre &a, const Livre &b) -> bool {
        return a.getAnnee() < b.getAnnee();
    });
}

void Bibliotheque::ecrireFichier(const std::string &nomFichier) const
{
    std::ofstream file;
    file.open(nomFichier);
    std::vector<Livre>::const_iterator iterator = this->begin();
    while (iterator != this->end())
    {
        std::stringstream s;
        s << *iterator;
        file << s.str() << std::endl;
        iterator++;
    }
}

void Bibliotheque::lireFichier(const std::string &nomFichier)
{
    std::fstream fs(nomFichier, std::ios::in);

    if (fs.fail()) {
        throw std::string("erreur : lecture du fichier impossible");
    }

    Livre l;
    fs >> l;
    while (!fs.eof()) {
        push_back(l);
        fs >> l;
    }

    fs.close();
}
