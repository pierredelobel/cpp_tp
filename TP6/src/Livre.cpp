#include "Livre.hpp"
#include <string>

Livre::Livre()
{}

Livre::Livre(const std::string & titre, const std::string & auteur, int annee) : _titre(titre), _auteur(auteur), _annee(annee)
{
    if (auteur.find(";") != std::string::npos) {
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    } else if (titre.find(";") != std::string::npos) {
        throw std::string("erreur : titre non valide (';' non autorisé)");        
    } else if (auteur.find("\n") != std::string::npos) {
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    } else if (titre.find("\n") != std::string::npos) {
        throw std::string("erreur : titre non valide ('\n' non autorisé)");        
    }
    
    
}

const std::string & Livre::getTitre() const
{
    return this->_titre;
}

const std::string & Livre::getAuteur() const
{
    return this->_auteur;
}

int Livre::getAnnee() const
{
    return this->_annee;
}

bool Livre::operator<(const Livre & livre) const
{
    
    if (livre.getAuteur() == this->getAuteur()) {
        if (livre.getTitre() < this->getTitre()) {
            return false;
        } else if (livre.getTitre() == this->getAuteur()) {
            return true;
        } else if (livre.getTitre() > this->getTitre()) {
            return true;
        } else {
            return false;
        }
    } else if (livre.getAuteur() < this->getAuteur()) {
        return false;
    } else {
        return true;
    } 
}

bool Livre::operator==(const Livre & livre) const
{
    if (this->getAuteur() != livre.getAuteur()) {
        return false;
    } else if (this->getTitre() != livre.getTitre()) {
        return false;
    } else if (this->getAnnee() != livre.getAnnee()) {
        return false;
    } else {
        return true;
    } 
}

std::ostream& operator<<(std::ostream &s, Livre const& livre)
{
    s << livre._titre << ";" << livre._auteur << ";" << std::to_string(livre._annee);
    return s;
}

std::istream& operator>>(std::istream &s, Livre & livre)
{
    // TODO
    std::getline(s, livre._titre, ';');
    if (livre._titre == "") { return s;  }

    std::getline(s, livre._auteur, ';');
    std::string annee;
    std::getline(s, annee);


    livre._annee = stoi(annee);

    return s;
    
}

