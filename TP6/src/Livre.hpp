#ifndef LIVRE_HPP
#define LIVRE_HPP
#include <iostream>
#include <sstream>
class Livre
{
private:
    std::string _titre;
    std::string _auteur;
    int _annee;
public:
    Livre();
    Livre(const std::string & titre, const std::string & auteur, int annee);
    const std::string & getTitre() const;
    const std::string & getAuteur() const;
    int getAnnee() const;
    virtual bool operator<(const Livre & livre) const;
    virtual bool operator==(const Livre & livre) const;
    
    friend std::ostream& operator<<(std::ostream &s, Livre const& livre);
    friend std::istream& operator>>(std::istream &s, Livre & livre);
};

std::ostream& operator<<(std::ostream &s, Livre const& livre);
std::istream& operator>>(std::istream &s, Livre & livre);

#endif
