#include "Image.hpp"
#include <fstream>
#include <cmath>

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur) {
    this->_pixels = new int[largeur * hauteur];
}

Image::~Image()
{
    delete[] this->_pixels;
}

int& Image::getHauteur()
{
    return this->_hauteur;
}

int& Image::getLargeur()
{
    return this->_largeur;
}

int& Image::getPixel(int i, int j)
{
    return this->_pixels[j + this->getLargeur() * i];
}

void Image::setPixel(int i, int j, int couleur)
{
    this->_pixels[j + this->getLargeur() * i] = couleur;
}

void Image::ecrirePnm(const std::string & nomFichier)
{
    std::ofstream file;
    file.open(nomFichier);
    file << "P2" << std::endl;
    file << this->_largeur << " " << this->_hauteur << std::endl;
    file << 255 << std::endl;
    for(int i = 0; i < this->_largeur; i++)
    {
        for(int j = 0; j < this->_hauteur; j++)
        {
            file << this->getPixel(i, j) << " ";
        }
        file << std::endl;
    }
    file.close();
}

void Image::remplir()
{
    float tour = this->getLargeur() / 4 / (3.14f * 2);  // On fait 4 tours du cosinus sur la largeur de l'image

    for(int i = 0; i < this->getLargeur(); i++)
    {
        for(int j = 0; j < this->getHauteur(); j++)
        {
            this->setPixel(i, j, round((cos(j/tour) +1)*(255/2.f)));
        }
    }
}
