#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <iostream>

class Image
{
private:
    int _largeur;
    int _hauteur;
    int * _pixels;
public:
    Image(int largeur, int hauteur);
    ~Image();
    int& getLargeur();
    int& getHauteur();
    int& getPixel(int i, int j);
    void setPixel(int i, int j, int couleur);
    void ecrirePnm(const std::string & nomFichier);
    void remplir();
};

#endif

