#include "Image.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Image_test1)  {
    Image i(10, 10);
    i.setPixel(2, 2, 4);
    CHECK_EQUAL(i.getPixel(2, 2), 4);
}

TEST(GroupImage, Image_test2)  {
    Image i(10, 10);
    CHECK_EQUAL(i.getHauteur(), 10);
}

TEST(GroupImage, Image_test3)  {
    Image i(10, 10);
    CHECK_EQUAL(i.getLargeur(), 10);
}

