#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    //this->chargerInventaire();
    for (auto & v : _vues)
      v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte() const
{
    std::ostringstream oss;
	// TODO decommenter
	oss << this->_inventaire;
    return oss.str();
}

void Controleur::chargerInventaire(const std::string & nomFichier)
{
    std::fstream fs(nomFichier, std::ios::in);
    std::string nom, date, volume;
    if (fs.fail()) {
        throw std::string("erreur : lecture du fichier impossible");
    }

    while (!fs.eof()) {
        getline(fs, nom, ';');
        getline(fs, date, ';');
        getline(fs, volume, ';');
        this->_inventaire._bouteilles.push_back(Bouteille{nom, date, std::stof(volume)});

    }

    fs.close();
    for (auto & v : _vues)
        v->actualiser();
}




