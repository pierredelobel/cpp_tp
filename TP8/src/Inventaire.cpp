#include "Inventaire.hpp"

std::ostream& operator<<(std::ostream &s, Inventaire const& inventaire)
{
    std::vector<Bouteille>::const_iterator iterator = inventaire._bouteilles.begin();
    while (iterator != inventaire._bouteilles.end())
    {
        s << iterator->_nom << ";" << iterator->_date << ";" << iterator->_volume << std::endl;
        ++iterator;
    }
    return s;
}

std::istream& operator>>(std::istream &s, Inventaire const& inventaire)
{
    
    std::getline(s, livre._titre, ';');
    if (livre._titre == "") { return s;  }

    std::getline(s, livre._auteur, ';');
    std::string annee;
    std::getline(s, annee);


    livre._annee = stoi(annee);

    return s;
}


