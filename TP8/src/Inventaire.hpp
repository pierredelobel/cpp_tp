#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <sstream>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::vector<Bouteille> _bouteilles;

    public:
        friend std::ostream& operator<<(std::ostream &s, Inventaire const& inventaire);
        friend std::istream& operator>>(std::istream &s, Inventaire const& inventaire);
};

std::ostream& operator<<(std::ostream &s, Inventaire const& inventaire);
std::istream& operator>>(std::istream &s, Inventaire const& inventaire);


#endif
